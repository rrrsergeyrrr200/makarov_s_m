#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <string>
#include <vector>
#include "opencv2/opencv.hpp"
#include "../include/LaneDetector.hpp"
#include "../LaneDetector/LaneDetector.cpp"


int main(int argc, char *argv[]) {
	setlocale(LC_ALL, "Russian");
	std::cout << "������� ������ ���� �� ������������ �����\n";
    std::string source;
	std::cin >> source;
    cv::VideoCapture cap(source);
    if (!cap.isOpened())
      return -1;

    LaneDetector lanedetector; 
    cv::Mat frame;
    cv::Mat img_denoise;
    cv::Mat img_edges;
    cv::Mat img_mask;
    cv::Mat img_lines;
    std::vector<cv::Vec4i> lines;
    std::vector<std::vector<cv::Vec4i> > left_right_lines;
    std::vector<cv::Point> lane;
	cv::Point intersect_point;
	float IoU = 0.0;
    int flag_plot = -1;
    int i = 0;
	int j = 0;
	double vanish_x;

	cv::Point2i standardPoints[16][3] = { 
		{cv::Point2i(461,720),cv::Point2i(1125,720),cv::Point2i(734,406)},
		{cv::Point2i(494,720),cv::Point2i(1233,720),cv::Point2i(724,407)},
		{cv::Point2i(483,720),cv::Point2i(1171,720),cv::Point2i(724,407)},
		{cv::Point2i(490,720),cv::Point2i(1179,720),cv::Point2i(722,401)},
		{cv::Point2i(566,720),cv::Point2i(1258,720),cv::Point2i(717,405)},
		{cv::Point2i(567,720),cv::Point2i(1279,720),cv::Point2i(716,407)},
		{cv::Point2i(428,720),cv::Point2i(1171,720),cv::Point2i(716,403)},
		{cv::Point2i(551,720),cv::Point2i(1219,720),cv::Point2i(716,405)},
		{cv::Point2i(491,720),cv::Point2i(1174,720),cv::Point2i(715,406)},
		{cv::Point2i(490,720),cv::Point2i(1171,720),cv::Point2i(716,406)},
		{cv::Point2i(479,720),cv::Point2i(1203,720),cv::Point2i(720,404)},
		{cv::Point2i(492,720),cv::Point2i(1178,720),cv::Point2i(717,404)},
		{cv::Point2i(623,720),cv::Point2i(1276,720),cv::Point2i(714,399)},
		{cv::Point2i(589,720),cv::Point2i(1279,720),cv::Point2i(711,401)},
		{cv::Point2i(494,720),cv::Point2i(1218,720),cv::Point2i(721,400)},
		{cv::Point2i(561,720),cv::Point2i(1276,720),cv::Point2i(722,407)}
	};

    
    while (i < 540) {

      if (!cap.read(frame))
        break;

      img_denoise = lanedetector.deNoise(frame);
      
      img_edges = lanedetector.edgeDetector(img_denoise);
      
      img_mask = lanedetector.mask(img_edges);
	  
      lines = lanedetector.houghLines(img_mask);

      if (!lines.empty()) {
        left_right_lines = lanedetector.lineSeparation(lines, img_edges);

        lane = lanedetector.regression(left_right_lines, frame);
		
		intersect_point = lanedetector.vanishPoint();

        flag_plot = lanedetector.plotLane(frame, lane);

		if (i % 10 == 0) {
			IoU += lanedetector.IoU(lane, intersect_point, standardPoints, j);
			j++;
		}
		
        i += 1;
        cv::waitKey(25);
      } else {
          flag_plot = -1;
      }
    }
	IoU /= j + 1;
	std::cout << IoU << std::endl;
    return flag_plot;
}
